import java.util.ArrayList;

public class StringCalculator {

	public int Add(String numbers) throws NumberFormatException {
		char delimiter = ',';
		if(numbers.contains("//")) {
			delimiter = numbers.charAt(2);
			numbers = numbers.substring(4);
		}
		ArrayList<Integer> negativeNumbers = new ArrayList<>();
		String[] numberArray = numbers.split("[\n"+delimiter+"]");
		int sum=0;
		for (String number : numberArray) {
			try {
				int n = Integer.parseInt(number);
				if(n>=0) {
					sum+=n;
				}else {
					negativeNumbers.add(n);
				}
				
			} catch (NumberFormatException e) {
				
			} catch (Exception e) {
				
			}
			
		}
		if(negativeNumbers.size()>0) {
			throw new NumberFormatException("The following negative numbers are not allowed: "+negativeNumbers.toString());
		}
		return sum;
	}
}
