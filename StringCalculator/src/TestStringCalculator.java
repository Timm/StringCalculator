import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class TestStringCalculator {

	@Test
	void testAddEmpty() {
		StringCalculator stringCalculator = new StringCalculator();
		int expected = 0;
		int actual = stringCalculator.Add("");
		assertEquals(expected, actual);
	}
	
	@Test
	void testAddOneZero() {
		StringCalculator stringCalculator = new StringCalculator();
		int expected = 1;
		int actual = stringCalculator.Add("1,0");
		assertEquals(expected, actual);
	}
	
	@Test
	void testAddZero() {
		StringCalculator stringCalculator = new StringCalculator();
		int expected = 0;
		int actual = stringCalculator.Add("0");
		assertEquals(expected, actual);
	}
	
	@Test
	void testAddOne() {
		StringCalculator stringCalculator = new StringCalculator();
		int expected = 1;
		int actual = stringCalculator.Add("1");
		assertEquals(expected, actual);
	}
	
	@Test
	void testAddOneEmpty() {
		StringCalculator stringCalculator = new StringCalculator();
		int expected = 1;
		int actual = stringCalculator.Add("1,");
		assertEquals(expected, actual);
	}
	
	@Test
	void testAddOneThree() {
		StringCalculator stringCalculator = new StringCalculator();
		int expected = 4;
		int actual = stringCalculator.Add("1,3");
		assertEquals(expected, actual);
	}
	
	@Test
	void testAddEmptyThree() {
		StringCalculator stringCalculator = new StringCalculator();
		int expected = 3;
		int actual = stringCalculator.Add(",3");
		assertEquals(expected, actual);
	}
	

	@Test
	void testAddEmptyEmpty() {
		StringCalculator stringCalculator = new StringCalculator();
		int expected = 0;
		int actual = stringCalculator.Add(",");
		assertEquals(expected, actual);
	}
	
	@Test
	void testAddManyNumbers() {
		StringCalculator stringCalculator = new StringCalculator();
		int expected = 6;
		int actual = stringCalculator.Add("0,1,2,3");
		assertEquals(expected, actual);
	}
	
	@Test
	void testAddNewline() {
		StringCalculator stringCalculator = new StringCalculator();
		int expected = 7;
		int actual = stringCalculator.Add("3\n4");
		assertEquals(expected, actual);
	}
	
	@Test
	void testAddNewlineOrComma() {
		StringCalculator stringCalculator = new StringCalculator();
		int expected = 6;
		int actual = stringCalculator.Add("1\n2,3");
		assertEquals(expected, actual);
	}
	

	@Test
	void testAddDelimiter() {
		StringCalculator stringCalculator = new StringCalculator();
		int expected = 3;
		int actual = stringCalculator.Add("//;\n1;2");
		assertEquals(expected, actual);
	}
	
	@Test
	void testAddNegativeNumberException() {
		StringCalculator stringCalculator = new StringCalculator();
		Exception exception = assertThrows(NumberFormatException.class, () -> {
			stringCalculator.Add("-1,2,3,0,-5");
	    });

	    String expectedMessage = "The following negative numbers are not allowed: [-1, -5]";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
}
